API_KEY = 'AIzaSyAok21Kv66znlqLQQEhqGWXJmu5blOuMqI'

require 'net/http'

class ResultsController < ApplicationController

  def show
    if params[:q] == ''
      redirect_to root_path
    else
      @results = []
      query = params[:q]
      query = keyword query
      interesting_spots(query.downcase)
      parks(query.downcase)
      restaurants(query.downcase)
      companies(query.downcase)
      zaps(query.downcase)

      if @results.count == 0
        redirect_to root_path
      end
    end
  end

  private
  def interesting_spots(query)
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    categories = db['attraits_categories.json'].find
    categories.each { |category|
      category[:Nom].split(' ').each { |name|
        if query.include? name.downcase
          r = db['attraits_touristique.json'].find({Categories: category[:ID].to_s})
          r.each { |c|
            @results << {
                id: c[:_id],
                name: c[:Nom],
                full_address: "#{c[:NumeroCivique]} #{c[:Rue]}, #{c[:Ville]}, #{c[:CodePostal]}",
                address: "#{c[:NumeroCivique]} #{c[:Rue]}",
                city: c[:Ville],
                zip: c[:CodePostal],
                website: c[:SiteWeb],
                phone: c[:NumeroTelephone],
                longitude: c[:Longitude],
                latitude: c[:Latitude],
                description: c[:DescriptionCourte],
                image: c[:FichierImage]
            }
          }
        end
      }
    }
    db.close
  end

  def restaurants(query)
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    categories = db[:'restaurant_categories.json'].find
    categories.each { |category|
      category[:Nom].split(' ').each { |name|
        if query.include? name.downcase
          r = db['restaurant_detail.json'].find({Categories: category[:ID].to_s})
          r.each { |c|
            @results << {
                id: c[:_id],
                name: c[:Nom],
                address: "#{client.nil? c[:NumeroCivique]} #{c[:Rue]}",
                city: c[:Ville],
                zip: c[:CodePostal],
                website: c[:SiteWeb],
                phone: c[:NumeroTelephone],
                longitude: c[:Longitude],
                latitude: c[:Latitude],
                description: c[:DescriptionCourte],
                image: c[:FichierImage]
            }
          }
        end
      }
    }
    db.close
  end

  def parks(query)
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    entries = db['structure_recreative_nogeo.json'].find
    entries.each { |entry|
      names = [entry[:TYPE_SR], entry[:NOM_SR], entry[:INFO]]
      names.each { |name|
        unless name.nil?
          name.split(' ').each { |park|
            if query.include? park.downcase
              @results << {
                  id: entry[:_id],
                  name: entry[:NOM_SR],
                  address: entry[:AD],
                  city: entry[:AD_MUN],
                  zip: nil,
                  website: entry[:URL],
                  phone: entry[:TEL1],
                  longitude: nil,
                  latitude: nil,
                  description: entry[:INFO],
                  image: nil
              }
            end
          }
        end
      }
    }
    db.close
  end

  def zaps(query)
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    entries = db['zap.json'].find.skip(1)
    entries.each { |entry|
      names = [entry[:FIELD2], entry[:FIELD3]]
      names.each { |name|
        unless name.nil?
          name.split(' ').each { |zap|
            if query.include? zap.downcase
              @results << {
                  id: entry[:_id],
                  name: entry[:FIELD2],
                  address: "#{entry[:FIELD4]} #{entry[:FIELD5]}",
                  city: entry[:FIELD6],
                  zip: entry[:FIELD7],
                  website: nil,
                  email: entry[:FIELD11],
                  phone: entry[:FIELD10],
                  longitude: entry[:FIELD13],
                  latitude: entry[:FIELD12],
                  description: entry[:FIELD3],
                  image: nil
              }
            end
          }
        end
      }
    }
    db.close
  end

  def companies(query)
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    entries = db['commerce_nogeo.json'].find
    entries.each { |entry|
      name = entry[:TYPE_SR]
      unless name.nil?
        name.split(' ').each { |companie|
          if query.include? companie.downcase
            @results << {
                id: entry[:_id],
                name: entry[:NOM_SR],
                address: entry[:AD],
                city: entry[:AD_MUN],
                zip: nil,
                website: entry[:URL],
                phone: entry[:TEL1],
                longitude: nil,
                latitude: nil,
                description: nil,
                image: nil
            }
          end
        }
      end
    }
    db.close
  end

  def keyword(keywords)
    main_language = 'fr'
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    word = translate(main_language, keywords).downcase
    query = db[:keywords].find({name: word})
    if query.count != 1
      db[:keywords].insert_one({name: word, weight: 0})
    end
    db[:keywords].update_one({name: word}, {:$inc => {weight: 1}})
    db.close
    unless word.end_with? 'en.fr'
      query = db[:keywords].find({name: word})
      if query.count != 1
        db[:keywords].insert_one({name: word, weight: 0})
      end
      db[:keywords].update_one({name: word}, {:$inc => {weight: 1}})
    end
    @result = word
  end

  def generatewordcloud
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    query = db['attraits_touristique.json']
    array = Array.new
    query.find.each do |value|
      puts value[:Nom].encode('utf-8')
      array.insert(0, **value[:Nom].split(' '))
      array.insert(0, **value[:DescriptionCourte].split(' '))
    end
    db.close
    @result = array
  end

  def search
    params[:id]
  end

  def geocode(address)
    coord = JSON.parse(Net::HTTP.get(URI("https://maps.googleapis.com/maps/api/geocode/json?address=#{address.replace(' ', '+')}&key=#{API_KEY}")))[:result][:geometry][:location]
    return coord[0], coord[1]
  end
end
