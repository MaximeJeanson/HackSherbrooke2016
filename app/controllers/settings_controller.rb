class SettingsController < ApplicationController
  def show
    render json: settings
  end

  def update
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    settings = ActiveSupport::JSON.decode(request.body.read)

    db[:users].update_one({user_id: current_user}, {:$set => settings})
    db.close
    render text: ''
  end

  def create
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    settings = ActiveSupport::JSON.decode(request.body.read)

    db[:users].insert_one({user_id: current_user}.merge settings)
    db.close
    render text: ''
  end
end
