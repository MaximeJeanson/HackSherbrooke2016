class SearchController < ApplicationController
  def index
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    settings
    @results = []
    result = db[:keywords].find
    puts result.count
    result.each {|query|
      @results << {
          text: query[:name],
          weight: query[:weight]
      }
    }
    db.close
  end
end
