require 'securerandom'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
    unless cookies.has_key? :user_id
      cookies[:user_id] = SecureRandom::uuid.to_s
    end

    cookies[:user_id]
    unless session.has_key? :user_id
      session[:user_id] = SecureRandom::uuid.to_s
    end

    session[:user_id]
  end

  def settings
    db = Mongo::Client.new('mongodb://127.0.0.1:27017', database: :hack)
    p db[:users].find.count
    p current_user

    model = ActiveSupport::JSON.decode(ActiveSupport::JSON.encode(db[:users].find({user_id: current_user})))
    if model.count > 0
      model = model[0]
      @settings = {sex: model[:sex], country: model[:country], language: model[:language]}
    else
      @settings = {sex: 'male', country: 'CA', language: 'fr'}
    end
    db.close
  end

  def tr(text)
    settings
    translate @settings[:language], text
  end

  def translate(lang, text)
    source_lang = JSON.parse(Net::HTTP.get(URI("https://www.googleapis.com/language/translate/v2/detect?key=#{API_KEY}&q=#{text}")))[:data][:detections][0][0]

    cookies[:lang] = source_lang[:language]

    if source_lang[:language] == 'fr'
      return text
    end

    JSON.parse(Net::HTTP.get(URI("https://www.googleapis.com/language/translate/v2?key=#{API_KEY}&source=#{source_lang[:language]}&target=#{lang}&q=#{text}")))[:data][:translations][0][:translatedText]
  end
end
