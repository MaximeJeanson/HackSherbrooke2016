# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store, key: '_SherbrookeWorldwide_session'
Rails.application.config.session_store :cookie_store, user_id: SecureRandom::uuid.to_s
