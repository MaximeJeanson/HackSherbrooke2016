require 'mongo'
require 'json'

class CreateDatabase
  def initialize
    client = Mongo::Client.new('mongodb://127.0.0.1:27017', database: 'hack')
    client.database.drop

    Dir.glob('datas/*.json').each { |file|
      p file
      begin
        doc = JSON.parse(File.new(file, ).read)
        if doc.instance_of? Array
          client[File.basename(file)].insert_many(doc)
        else
          client[File.basename(file)].insert_one(doc)
        end
      end
    }

    client['attraits_categories.json'].indexes.create_one({Nom: 'text'}, name: 'text', unique: false)
    client['attraits_categories.json'].indexes.each do |index|
      p index
    end
    p client['attraits_categories.json'].find.count
  end
end

CreateDatabase.new()